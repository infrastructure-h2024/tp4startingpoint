from main.utils import pagebroker as pg
from main.utils import conversion as conv
from main.utils.exceptions import *
from main.memanalysis import meta as mp
from main.memanalysis import bitmap as bm
from main.memanalysis import pagetablepage as pagetable
from main.memanalysis import pagetableentry as entry
from main.memanalysis import processlookuppage as process

##
# Get the meta page from the dump and parse it
#
# Return: the parsed metapage
##
def getMetaPage(fileNumber):
	dumpMetaPage = pg.getPage(fileNumber, 0)
	metaPage = mp.parseFromHexDump(dumpMetaPage)
	
	return metaPage


##
# From the bitmap page, extract the pages in the memory dump that are currently in used
#
# Return: a list of all pages that are used
##
def getUsedPages(fileNumber):
	dump = pg.getPage(fileNumber, 0)
	metaPage = mp.parseFromHexDump(dump)
	nbBitmapPages = metaPage.getNbBitmapPages()
	pageSizeOctets = metaPage.getNbOctetsInPage()

	rep = []
	for pagePos in range(0, nbBitmapPages):
		dumpBitmapPage = pg.getPage(fileNumber, pagePos+1) #+1 is to skip over the Metapage
		bitmapPage = bm.parseFromHexDump(dumpBitmapPage, pageSizeOctets)
		rep.extend(bitmapPage.getAllUsedPages())
	
	return rep

##
#
#
#
##
def getPTPageInfo(fileNumber):
	metaPage = getMetaPage(fileNumber)
	nbPTPages = metaPage.getNbPageTablePages()
	pageSizeOctets = metaPage.getNbOctetsInPage()
	nbPTEntriesPerPage = pageSizeOctets // entry.ENTRY_SIZE
	locationOfFirstPTPage = 1 + metaPage.getNbBitmapPages()

	rep = dict()
	usedPages = getUsedPages(fileNumber)

	for page in usedPages:
		ptRelativeLocation = page // nbPTEntriesPerPage
		ptAbsoluteLocation = ptRelativeLocation + locationOfFirstPTPage
		dumpPTPage = pg.getPage(fileNumber, ptAbsoluteLocation)
		ptPage = pagetable.parseFromHexDump(dumpPTPage, pageSizeOctets, ptRelativeLocation)
		ptEntry = ptPage.getPTEntry(page%nbPTEntriesPerPage)
		rep[ptEntry.getPageID()] = ptEntry.toMap()

	return rep

##
# Get the page lookup tables pages from the meta information
#
# Return: the Page Lookup Tables pages
##
def getPLTPages(fileNumber):
	metaPage = getMetaPage(fileNumber)
	nbPTPages = metaPage.getNbPLTPages()
	pageSizeOctets = metaPage.getNbOctetsInPage()
	locationOfFirstPLTPage = 1 + metaPage.getNbBitmapPages() + metaPage.getNbPageTablePages()
	
	dumpPLTPage = pg.getPage(fileNumber, locationOfFirstPLTPage)
	pltPage = process.parseFromHexDump(dumpPLTPage, pageSizeOctets)

	return pltPage


def getPhysicalAddress(fileNumber, pid, vadd):
	fileName = pg.getDumpFileName(fileNumber)
	metaPage = getMetaPage(fileNumber)
	pltPage = getPLTPages(fileNumber)
	
	if not pltPage.isActiveProcess(pid):
		raise InactivePidException('The given PID (' + str(pid) + ') is not active')

	#Getting the pages for the requested pid
	pages = pltPage.getProcessPages(pid)
	nbPages = len(pages)

	#Checking that the requested virtual address is not segfaulting
	pageSizeOctets = metaPage.getNbOctetsInPage()
	whichPageRelative = vadd // pageSizeOctets
	if whichPageRelative >= nbPages:
		raise VirtualAddressOutsidePhysicalMemoryException('The virtual address provided (' + str(vadd) + ') falls outside the memory space of this program.')

	pageId = pages[whichPageRelative]
	physicalAddress = pageId * pageSizeOctets
	physicalAddress = physicalAddress + (vadd % pageSizeOctets)

	return conv.intToBase(physicalAddress, 16)

