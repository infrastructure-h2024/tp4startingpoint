from main.memanalysis import pagetableentry as pte

class PTPage:
	MAX_NB_ENTRIES_PTPAGE = 32 #Hardcoded max number of entries in a PTPage

	def __init__(self, ptPageId):
		self.__ptEntries = dict()
		offset = ptPageId * PTPage.MAX_NB_ENTRIES_PTPAGE
		for pos in range(0, PTPage.MAX_NB_ENTRIES_PTPAGE): 
			pageId = pos + offset
			ptEntry = pte.PTEntry(pageId, False, pageId, [])
			self.__ptEntries[pos] = ptEntry

	#WARNING: Should be used only by the Generator, not by students
	def setPTEntry(self, ptEntry, relativeLocation):
		#TODO make sure we are in the bounds
		if relativeLocation < 0:
			raise ValueError("Cannot set ptEntry to negative location (" + str(relativeLocation) + ").")
		elif relativeLocation >= PTPage.MAX_NB_ENTRIES_PTPAGE :
			raise ValueError("Cannot set ptEntry to location > " + str(PTPage.MAX_NB_ENTRIES_PTPAGE) + " current value is (" + str(relativeLocation) + ").")
		self.__ptEntries[relativeLocation] = ptEntry
#		self.__ptEntries[ptEntry.getPageID()] = ptEntry

	def getPTEntry(self, relativeLocation):
		return self.__ptEntries[relativeLocation]

	def pageIsSwappedOut(self, pageId):
		ptEntry = self.__ptEntries[pageId]
		#make sure we found something, we should always do... use a get method and validate the pageId as in set
		return ptEntry.isSwappedOut()

	def getPageLocation(self, pageId):
		ptEntry = self.__ptEntries[pageId]
		#make sure we found something, we should always do
		return ptEntry.getPageLocation()

	def processHasAccessToPage(self, pid, pageId):
		ptEntry = self.__ptEntries[pageId]
		#make sure we found something, we should always do
		return ptEntry.processHasAccessToPage(pid)

	def giveProcessAccessToPage(self, pid, pageId):
		ptEntry = self.__ptEntries[pageId]
		#make sure we found something, we should always do
		return ptEntry.giveProcessAccessToPage(pid)

def parseFromHexDump(hexDump, pageSizeOctets, ptOffset):
	if len(hexDump) != 2*pageSizeOctets: #we are in hex, hence x2
		raise ValueError("The size of the provided hexDump (" + str(len(hexDump))  + ") should be exactly a page size, i.e. (" + str(pageSizeOctets*2)  + ") hex digits.")
	
	ptPage = PTPage(ptOffset)
	pageId = 0
	ptEntrySizeOctets = pte.ENTRY_SIZE
	ptEntrySizeHexa = ptEntrySizeOctets*2
	nbPTEntriesPerPage = pageSizeOctets // ptEntrySizeOctets
	
	while pageId < nbPTEntriesPerPage:
		start = pageId*ptEntrySizeHexa
		end = start + ptEntrySizeHexa
		entryDump = hexDump[start:end]
		absolutePageId = pageId+(ptOffset*nbPTEntriesPerPage)
#		print("parsing PtPage " + str(ptOffset) + " relative pageId: " + str(pageId) + " absolute pageId: " + str(absolutePageId))
		entry = pte.parseFromHexDump(entryDump, absolutePageId)
		ptPage.setPTEntry(entry, pageId)
		pageId = pageId + 1
	return ptPage
