class BitmapPage:
	def __init__(self, nbPages):
		self.__nbPages__ = nbPages
		self.__bitmap__ = [0]*self.__nbPages__
		for pageId in range(0, self.__nbPages__):
			self.__bitmap__[pageId] = False

	def isPageUsed(self, pos):
		self.__validateBitmapPosition__(pos)
		pos = pos - 1 if pos != 0 else 0
		return self.__bitmap__[pos]

	def __setPageStatus__(self, pos, pageIsUsed):
		self.__validateBitmapPosition__(pos)
		self.__bitmap__[pos] = pageIsUsed

	def getAllUsedPages(self):
		usedPages = []
		for pageID in range(0, self.__nbPages__):
			if self.isPageUsed(pageID):
				usedPages.append(pageID)
		return usedPages

	def __validateBitmapPosition__(self, pos):
		if pos < 0:
			raise ValueError("The position provided (" + str(pos)  + ") is invalid to search/access any bitmap, must be >= 0.")

		if pos >= self.__nbPages__:
			raise ValueError("The position provided (" + str(pos)  + ") is invalid to search/access the current bitmap which contains (" + str(self.__nbPages__)  + ") elements.")


def parseFromHexDump(hexDump, sizeOfPagesOctets):
	
	if len(hexDump) < 2 * sizeOfPagesOctets:
		raise ValueError("BitmapPage: The size of the provided hexDump is smaller (" + str(len(hexDump)/2) + " octets) than a single page (" + str(sizeOfPagesOctets)  + " octets).")
	
	if len(hexDump) > 2 * sizeOfPagesOctets:
		raise ValueError("BitmapPage: The size of the provided hexDump is larger (" + str(len(hexDump)/2) + " octets) than a single page (" + str(sizeOfPagesOctets)  + " octets).")
	
	nbPages = sizeOfPagesOctets*8
	bitmapPage = BitmapPage(nbPages)
	count = 0
	
	for hexDigit in hexDump:
		if hexDigit == '0':
			count = count + 4
		else:
			binDump = bin(int(hexDigit, 16))[2:].zfill(4)
			for binDigit in binDump:
				if binDigit == '1':
					bitmapPage.__setPageStatus__(count, True)
				count = count + 1
				if count >= nbPages:
					return bitmapPage
		if count >= nbPages:
			return bitmapPage
