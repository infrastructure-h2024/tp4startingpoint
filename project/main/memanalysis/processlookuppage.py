from main import utils
from main.memanalysis import processlookupentry as plte

class PLTPage:

	def __init__(self):
		self.__pltEntries = dict()
		for pid in range(0,64): #Hardcoded max number of processes in a PLTPage
			pltEntry = plte.PLTEntry(False, pid, [])
			self.__pltEntries[pid] = pltEntry

	#WARNING: Should be used only by the Generator, not by students
	def setPLTEntry(self, pid, PLTEntry):
		self.__pltEntries[pid] = PLTEntry

	def isActiveProcess(self, pid):
		pltEntry = self.__pltEntries[pid]
		return pltEntry.isUsed()

	def processHasPage(self, pid, pageID):
		pltEntry = self.__pltEntries[pid]
		return pltEntry.hasPage(pageID)

	def addPageToProcess(self, pid, pageID):
		pltEntry = self.__pltEntries[pid]
		hasBeenModified =  pltEntry.addPage(pageID)

	def getProcessPages(self, pid):
		pltEntry = self.__pltEntries[pid]
		return pltEntry.getPages()

	def toMap(self):
		rep = dict()
		for pid in self.__pltEntries:
			pltEntry = self.__pltEntries[pid]
			if pltEntry.isUsed():
				rep[pid] = pltEntry.getPages()
		return rep

def parseFromHexDump(hexDump, pageSizeOctets):
	if len(hexDump) != 2*pageSizeOctets: 
		raise ValueError("The size of the provided hexDump should be exactly a page size, i.e. (" + str(pageSizeOctets)  + ") octets")
	pltPage = PLTPage()
	pid = 0
	sizePLTEntryOctets = plte.SIZE // 8
	sizePLTEntryHexa = sizePLTEntryOctets * 2
	nbPLTEntriesPerPage = pageSizeOctets // sizePLTEntryOctets

	while pid < nbPLTEntriesPerPage:
		start = pid*sizePLTEntryHexa
		end = start + sizePLTEntryHexa
		entryDump = hexDump[start:end]
		entry = plte.parseFromHexDump(entryDump, pid)
		pltPage.setPLTEntry(pid, entry)
		pid = pid + 1
	return pltPage
