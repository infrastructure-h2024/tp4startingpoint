from main.utils import conversion as conv


ENTRY_SIZE = 32 #octets

class PTEntry:
	MAX_NB_PROCESS = 62

	def __init__(self, pageID, isSwappedOut, pageLocation, acl):
		if pageID != pageLocation:
			raise ValueError("We currently support only a model where pageID (" + str(pageID) + ") == pageLocation (" + str(pageLocation) + ") and this is not the case here.")

		if isinstance(pageID, int) and isinstance(pageLocation, int): 
			self.__pageID = pageID
			self.__pageLocation = pageLocation
		else:
			raise ValueError("Parameters given to PTEntry constructor are not all of the proper type. Either pageID (" + str(pageID) + ") or pageLocation (" + str(pageLocation)  + ").")


		for pid in acl:
			if not isinstance(pid, int):
				raise ValueError("Parameters given to PTEntry constructor are not all of the proper type. In ACL, we have value (" + str(pid) + ") which is not acceptable.")
		self.__acl = acl


		self.__isSwappedOut = isSwappedOut



	def isSwappedOut(self):
		return self.__isSwappedOut

	def getPageID(self): 
		return self.__pageID

#TODO remove
	def processHasAccessToPage(self, pid):
		if pid == 0:
			return True #OS process has access to everything
		return pid in self.__acl

#TODO remove
	def giveProcessAccessToPage(self, pid):
		if not pid in self.__acl and not pid == 0:
			if len(self.__acl) < 62:
				self.__acl.append(pid)
			else:
				raise ValueError("The page (" + str(self.__pageID)  + ") is already accessible by (" + str(len(self.__acl))  + ") processes and this is the limit. Operation to give access to (" + str(pid) + ") has failed.") 
				#TODO should not be value error
		else:
			raise ValueError("Process (" + str(pid)  + ") already has access to page (" + str(self.__pageID)  + "). Giving it access is useless...") #might be harsh to raise here...

	def getPageLocation(self):
		return self.__pageLocation

	def toMap(self):
		rep = dict()
		#rep['pageID'] = self.getPageID()
		rep['isSwappedOut'] = self.isSwappedOut()
		rep['pageLocation'] = self.getPageLocation()
		processes = self.__acl.copy()
		processes.append(0)
		processes.sort()
		rep['ACL'] = processes
		return rep

def parseFromHexDump(hexDump, pageId):
	if len(hexDump) != ENTRY_SIZE * 2:
		raise ValueError("The provided hex Dump for a PTEntry is of the wrong size (" + str(len(hexDump) // 2)  + " octets), was expecting size (" + str(ENTRY_SIZE)  + " octets).")
	start = conv.hexToInt(hexDump[0:2])
	isSwappedOut = False
	if start > 127:
		isSwappedOut = True

	pageLocation = conv.hexToInt(hexDump[2:4])

	acl = []
	for i in range (0,62):
		startPos = 4 + i*2
		pid = conv.hexToInt(hexDump[startPos:startPos+2])
		if pid != 0:
			acl.append(pid)

	return PTEntry(pageId, isSwappedOut, pageLocation, acl)
