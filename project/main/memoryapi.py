import sys
from flask import *

from main.utils import conversion as conv
from main.utils.exceptions import *
from main import memorydomain as dom

app = Flask('memoryBackEnd')

@app.route('/metainfo/<file_number>')
def getMetaPageInfo(file_number): #pragma: no cover
	metaPage = dom.getMetaPage(file_number)
	return jsonify({'fileName':file_number, 'metaInfo':metaPage.toMap()})

@app.route('/bitmap/<file_number>')
def getBitmapPageInfo(file_number): #pragma: no cover
	rep = dom.getUsedPages(file_number)
	return jsonify({'fileName':file_number, "bitmapInfo":rep})


@app.route('/pagetable/<file_number>')
def getPTPageInfo(file_number): #pragma: no cover
	rep = dom.getPTPageInfo(file_number)	

	return jsonify({'fileName':file_number, 'ptInfo':rep})

@app.route('/processlookup/<file_number>')
def getPLTPageInfo(file_number): #pragma: no cover
	pltPage = dom.getPLTPages(file_number)

	return jsonify({'fileName':file_number, 'pltInfo':pltPage.toMap()})

@app.route('/physicaladdress/<file_number>')
def getPhysicalAddress(file_number): #pragma: no cover
	if 'pid' in request.args:
		pid = request.args['pid']
		#TODO make sure this is a int
		pid = int(pid)
	else:
		return make_response({'message': "Missing argument pid for route getPhysicalAddress."}, 400)
	if 'vadd' in request.args:
		vaddHex = request.args['vadd']
		#TODO make sure this is an hex string
		vadd = conv.hexToInt(vaddHex)
	else:
		return make_response({'message': "Missing argument vadd for route getPhysicalAddress."}, 400)

	try:
		physicalAddress = dom.getPhysicalAddress(file_number, pid, vadd)
	except InactivePidException as e:
		return make_response({'message': str(e)}, 422)
	except VirtualAddressOutsidePhysicalMemoryException as ve:		
		return make_response({'message': str(ve)}, 422)

	return jsonify({'fileName': file_number, 'physicalAddress': physicalAddress})


if __name__ == '__main__': #pragma: no cover
	if len(sys.argv) >= 2:
		try:
			app_port = int(sys.argv[1])
		except ValueError as ve:
			app_port = 5558

		app.run(debug=True, host='0.0.0.0', port=app_port)
	else:
		raise ValueError('No starting port for the application')
