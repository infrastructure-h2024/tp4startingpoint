import sys
import requests
from flask import *

app = Flask('memoryanalysisfe')
TEMPLATE_FOLDER = app.root_path + '/project/app/templates/'
BACK_END_IP = 'backend'
BACK_END_PORT = '5555'

def page_template(title, data):
	html1 = '''<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<title>Mémoire</title>
			</head>
			<body>				   
				<h1>'''
	html2 = '''</h1> <div> '''
	html3 = '''</div>

				<p> Retour à la <a href='/'>page d'accueil</a></p>

			</body>
			</html>'''
	
	return html1 + str(title) + html2 + str(data) + html3

def home_page(title, data):
	html1 = '''<!DOCTYPE html>
			<html lang="en">
			<head>			
				<meta charset="UTF-8">
				<title>Mémoire</title>
			</head>
			<body>
				<nav>
					<a href="/">Accueil</a>
					<a href="/metainfo" onclick="location.href=this.href+'/'+ fileNumber;return false;">Meta</a>        
					<a href="/bitmap" onclick="location.href=this.href+'/'+ fileNumber;return false;">Bitmap</a>   
					<a href="/pagetable" onclick="location.href=this.href+'/'+ fileNumber;return false;">Page Table</a>   
					<a href="/processlookup" onclick="location.href=this.href+'/'+ fileNumber;return false;">Process Lookup</a>
				</nav>
				<hr>   
				<h1>'''
	html2 = '''</h1>
				<div>
					<h3>Analyse du fichier</h3>
					<input type="text" id="fileNumber"/>
					<button type="button" onclick="myFunction()"> Soumettre </button>
				</div>
				<h3> Données </h3>
				<div> '''
	html3 = '''</div>
					
				<script>
					let fileNumber = 1
					function myFunction() {
  						fileNumber = document.getElementById("fileNumber").value;
					}
				</script>
			</body>
			</html>'''
	
	return html1 + str(title) + html2 + str(data) + html3


@app.route('/')
def welcome_page(): #pragma: no cover

	data = {'message': 'Bienvenue!'}	

	return home_page('Accueil', data)

@app.route('/metainfo/<file_number>')
def getMetaPageInfo(file_number): #pragma: no cover

	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/metainfo/' + file_number)

	data = None
	if response.status_code == 200:
		data = json.loads(response.content.decode('utf-8'))
	else:
		data = {'message': 'Impossible de contacter l\'api'}
	
	return page_template('Méta-Information', data)

@app.route('/bitmap/<file_number>')
def getBitmapPageInfo(file_number): #pragma: no cover
	
	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/bitmap/' + file_number)

	data = None
	if response.status_code == 200:
		data = json.loads(response.content.decode('utf-8'))
	else:
		data = {'message': 'Impossible de contacter l\'api'}
	
	return page_template('Bitmap', data)


@app.route('/pagetable/<file_number>')
def getPTPageInfo(file_number): #pragma: no cover
	
	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/pagetable/' + file_number)

	data = None
	if response.status_code == 200:
		data = json.loads(response.content.decode('utf-8'))
	else:
		data = {'message': 'Impossible de contacter l\'api'}
	
	return page_template('Page Table', data)

@app.route('/processlookup/<file_number>')
def getPLTPageInfo(file_number): #pragma: no cover

	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/processlookup/' + file_number)

	data = None
	if response.status_code == 200:
		data = json.loads(response.content.decode('utf-8'))
	else:
		data = {'message': 'Impossible de contacter l\'api'}
	
	return page_template('Process Lookup', data)

@app.route('/physicaladdress/<file_number>')
def getPhysicalAddress(): #pragma: no cover
	
	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/physicaladdress')

	data = None
	if response.status_code == 200:
		data = json.loads(response.content.decode('utf-8'))
	else:
		data = {'message': 'Impossible de contacter l\'api'}
	
	return page_template('Adresse Physique', data)


if __name__ == '__main__': #pragma: no cover
	if len(sys.argv) >= 2:
		try:
			app_port = int(sys.argv[1])
		except ValueError as ve:
			app_port = 5557

		app.run(debug=True, host='0.0.0.0', port=app_port)
	else:
		raise ValueError('No starting port for the application')
